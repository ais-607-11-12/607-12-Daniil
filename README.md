# 607-12-Даниил
Welcome to my projects repository. Here i'll be uploading my stuff such as like solving some programming problems/tasks/exercises/quests and so on...

## Other
This account and the repository are linked to the Surgut State University (SurGU, СурГУ) and also linked to the [big project repository](https://gitlab.com/ais-607-11-12)

## Code Usage
The repository is public and opened to everyone. Feel free to take (or steal) my code to use it for personal purposes.
Simply download a file with the completed task then compile&run it.
