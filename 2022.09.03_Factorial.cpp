--
-- Author: Daniil Gorbunov
-- Group: 607-12
-- Date: 2022.09.03 14:56
-- Repository: https ://gitlab.com/gorbunov_dm/607-12-Daniil
-- Revision: 1
-- 

#include <iostream>
using namespace std;

int factorial(int a) {
    int b = 1;
    if (a >= 0) {
        for (int i = 1; i <= a; ++i)
            b *= i;
        return b;
    }
    else {
        cout << "Error: argument got <0 value." << endl;
        return 0;
    }
}

void FactorialTest() {
    int in = 0;
    int out = 0;
    int correct = 0;
    bool result = false;

    for (int i = 1; i < 10+1; i++) {
        in = i;
        switch (i) {
        case(1):
            out = factorial(in);
            correct = 1;
            //cout << "in: " << in << ", out: " << out << ", correct: " << correct << ", result: " << result << endl;
        case(2):
            out = factorial(in);
            correct = 2;
        case(3):
            out = factorial(in);
            correct = 6;
        case(4):
            out = factorial(in);
            correct = 24;
        case(5):
            out = factorial(in);
            correct = 120;
        case(6):
            out = factorial(in);
            correct = 720;
        case(7):
            out = factorial(in);
            correct = 5040;
        case(8):
            out = factorial(in);
            correct = 40320;
        case(9):
            out = factorial(in);
            correct = 362880;
        case(10):
            out = factorial(in);
            correct = 3628800;
        default:
            correct = 0;
        }

        if (out == correct)
            result = true;

        if (result) {
            cout << "[OK]   :: ";
        }
        else {
            cout << "[FAIL] :: ";
        }

        cout << "[" << i << "]" << "; Function: " << out << "; Correct: " << correct << endl;
    }
}

int main() {

    //cout << factorial(6) << endl;
    FactorialTest();

    return 0;
}